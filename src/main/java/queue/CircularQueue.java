package queue;

public class CircularQueue implements CircularQueueI {
    private int[] elements;
    private final int CAPACITY;
    private int tail = 0;
    private int head = 0;
    private int size = 0;

    public CircularQueue(int CAPACITY) {
        this.CAPACITY = CAPACITY;
        elements = new int[CAPACITY];
    }

    @Override
    public boolean enqueue(int element) {
        if (size != CAPACITY) {
            elements[tail] = element;
            tail = (tail + 1) % CAPACITY;
            size++;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int dequeue() {
        if (size > 0) {
            int top = elements[head];
            elements[head] = Integer.MIN_VALUE;
            head = (head + 1) % CAPACITY;
            size--;
            return top;
        } else {
            return Integer.MIN_VALUE;
        }
    }

    @Override
    public int front() {
        if (size > 0) {
            return elements[head];
        }
        return Integer.MIN_VALUE;

    }

    @Override
    public int size() {
        return size;
    }

    public int[] getElements() {
        return elements;
    }

    @Override
    public boolean isEmpty() {
        return (size == 0);
    }

    @Override
    public void bulkAdd(int[] inp) {
        // Handle empty inp array
        for(int i = 0; i < inp.length; i++) {
            enqueue(inp[i]);
        }
    }
}
