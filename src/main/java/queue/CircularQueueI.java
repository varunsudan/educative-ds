package queue;

public interface CircularQueueI {
    boolean enqueue(int element);
    int dequeue();
    int front();
    int size();
    boolean isEmpty();
    void bulkAdd(int[] inp);
}
