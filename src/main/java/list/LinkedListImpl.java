package list;

public class LinkedListImpl implements LinkedList {

    private ListNode head;
    private ListNode tail;
    private int size;

    public LinkedListImpl(ListNode head) {
        this.head = head;
    }

    public LinkedListImpl() {
    }

    @Override
    public ListNode getHead() {
        return head;
    }

    @Override
    public ListNode getTail() {
        return tail;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public String[] print() {
        ListNode current = head;
        if(isEmpty()) return null;
        String[] result = new String[size];
        int i = 0;
        while(current != null) {
            result[i++] = ":"+current.getEle();
            current = current.getNext();
        }
        return result;
    }

    @Override
    public boolean add(int ele) {
        final ListNode temp = new ListNodeImpl(ele);
        if(isEmpty()) {
            head = temp;
            tail = temp;
            size++;
            return true;
        }
        tail.setNext(temp);
        tail = temp;
        size++;
        return true;
    }

    @Override
    public boolean delete(int ele) {
        boolean isFound = false;
        if(!isEmpty()) {
            ListNode prev = null;
            ListNode curr = head;

            while(curr != null) {
                if(curr.getEle() == ele) {
                    isFound = true;
                    if(prev != null) {
                        prev.setNext(curr.getNext());
                        curr.setNext(null);
                        if(prev != null) {
                            curr = prev.getNext();
                        }
                    } else {
                        head = curr.getNext();
                        curr.setNext(null);
                        if(head != null) {
                            curr = head.getNext();
                        }
                    }
                    size--;
                } else {
                    curr = curr.getNext();
                }
            }
        }
        return isFound;
    }

    @Override
    public long size() {
        return size;
    }

    @Override
    public int search(int ele) {
        int eleFoundAt = 0;
        ListNode curr = head;
        while(curr != null) {
            if(curr.getEle() ==  ele)  return eleFoundAt;
            curr = curr.getNext();
            eleFoundAt++;
        }
        return -1;
    }

    @Override
    public LinkedList bulkAdd(int[] inp) {
        for(int ele : inp) {
            this.add(ele);
        }
        return this;
    }

}
