package list;

public interface ListNode {
    int getEle();
    ListNode getNext();
    boolean setNext(ListNode tail);
}
