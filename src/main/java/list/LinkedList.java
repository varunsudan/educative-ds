package list;

public interface LinkedList {
    ListNode getHead();
    ListNode getTail();
    boolean isEmpty();
    String[] print();
    boolean add(int ele);
    boolean delete(int ele);
    long size();
    int search(int ele);
    LinkedList bulkAdd(int[] inp);
}
