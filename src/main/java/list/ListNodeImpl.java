package list;

public class ListNodeImpl implements  ListNode {
    private int ele;
    private ListNode next;

    public ListNodeImpl(int ele) {
        this.ele = ele;
        next = null;
    }

    @Override
    public int getEle() {
        return ele;
    }

    @Override
    public ListNode getNext() {
        return next;
    }

    @Override
    public boolean setNext(ListNode node) {
        next = node;
        return true;
    }

}
