package set;

import java.util.List;

public class SetVariableSizeImpl {

    private List<Integer> elements;
    private int size = 0;

    public boolean add(int inp) {
        if(elements.stream().filter(x -> x == inp).findAny().isEmpty()) {
            return elements.add(inp);
        }
        return false;
    }

    public boolean remove(int inp) {
        for(int i = 0; i < elements.size(); i++) {
            if(elements.get(i) == inp) {
                elements.remove(i);
                return true;
            }
        }
        return false;
    }

    public boolean has(int inp) {
        return elements.stream().filter(x -> x == inp).findAny().isPresent();
    }

    public int size() {
        return size;
    }
}
