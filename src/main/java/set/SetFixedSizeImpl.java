package set;

import java.util.Arrays;

public class SetFixedSizeImpl {
    private final int CAPACITY;
    private Integer[] elements;
    private int searchUpToIndex = 0;
    private int size = 0;

    public SetFixedSizeImpl(int CAPACITY) {
        this.CAPACITY = CAPACITY;
        elements = new Integer[CAPACITY];
        Arrays.fill(elements, null);
    }

    public boolean add(int inp) {
        if (size != CAPACITY) {
            for (int i = 0; i < searchUpToIndex; i++) {
                if (elements[i] != null && elements[i] == inp) return false;
            }
            size++;
            elements[searchUpToIndex++] = inp;
            return true;
        }
        return false;
    }

    public boolean remove(int inp) {
        for(int i = 0; i  < elements.length; i++) {
            if(elements[i] != null && elements[i] == inp) {
                elements[i] = null;
                size--;
                return true;
            }
        }
        return false;
    }

    public boolean has(int inp) {
        for(int i = 0; i  < elements.length; i++) {
            if(elements[i] != null && elements[i] == inp) {
                return true;
            }
        }
        return false;
    }

    public int size() {
        return size;
    }
}
