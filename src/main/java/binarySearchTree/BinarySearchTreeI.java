package binarySearchTree;

public interface BinarySearchTreeI {
    BSTNode search(int value);
    BinarySearchTree insert(int value);
    BinarySearchTree bulkAdd(int[] inp);
    void inOrderTraversal();
    void preOrderTraversal();
    void postOrderTraversal();
}
