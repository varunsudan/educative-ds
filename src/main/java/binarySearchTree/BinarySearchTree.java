package binarySearchTree;

public class BinarySearchTree implements BinarySearchTreeI {

    private BSTNode root;

    public BinarySearchTree(BSTNode root) {
        this.root = root;
    }

    public BinarySearchTree() {
    }

    public BSTNode getRoot() {
        return root;
    }

    @Override
    public BSTNode search(int value) {
        BSTNode curr = root;

        if (root != null) {
            while (curr != null) {
                if (curr.getData() == value) {
                    return curr;
                } else if (value < curr.getLeft().getData()) {
                    curr = curr.getLeft();
                } else {
                    curr = curr.getRight();
                }
            }
        }

        return curr;
    }

    @Override
    public BinarySearchTree insert(int value) {
        BSTNode newNode = new BSTNode(value);
        BSTNode curr = root;
        BSTNode parent = null;

        if (root != null) {
            while (curr != null) {
                parent = curr;
                if (value < curr.getLeft().getData()) {
                    curr = curr.getLeft();
                } else {
                    curr = curr.getRight();
                }
            }
            if (value < parent.getLeft().getData()) {
                parent.setLeft(newNode);
            } else {
                parent.setRight(newNode);
            }
        } else {
            root = newNode;
        }

        return this;
    }

    @Override
    public BinarySearchTree bulkAdd(int[] inp) {
        // Validate Input & throw InvalidInput Exception
        for (int i = 0; i < inp.length; i++) {
            insert(i);
        }
        return this;
    }

    @Override
    public void inOrderTraversal() {
        inOrder(this.root);
    }

    private void inOrder(BSTNode root) {
        if (root != null) {
            System.out.println(root.getData());
            if (root.getLeft() != null) {
                inOrder(root.getLeft());
            }
            if (root.getRight() != null) {
                inOrder(root.getRight());
            }
        }
    }


    @Override
    public void preOrderTraversal() {
        preOrder(this.root);
    }

    private void preOrder(BSTNode root) {
        if (root != null) {
            if (root.getLeft() != null) {
                inOrder(root.getLeft());
            }
            if (root.getRight() != null) {
                inOrder(root.getRight());
            }
            System.out.println(root.getData());
        }
    }

    @Override
    public void postOrderTraversal() {
        preOrder(this.root);
    }

    private void postOrder(BSTNode root) {
        if (root != null) {
            System.out.println(root.getData());
            if (root.getLeft() != null) {
                inOrder(root.getLeft());
            }
            if (root.getRight() != null) {
                inOrder(root.getRight());
            }
        }
    }
}
