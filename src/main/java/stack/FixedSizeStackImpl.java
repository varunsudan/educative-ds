package stack;

import java.util.Arrays;

public class FixedSizeStackImpl implements StackI {
    private int CAPACITY;
    private int insertAtPos;
    private int[] elements;
    int size = 0;

    public FixedSizeStackImpl(int size) {
        if (size <= 0) throw new IllegalArgumentException("SIZE <=0 ");
        this.CAPACITY = size;
        elements = new int[size];
    }

    @Override
    public boolean push(int ele) {
        if (size != CAPACITY) {
            elements[insertAtPos++] = ele;
            size++;
            return true;
        } else {
            return false;
        }

    }

    @Override
    public int pop() {
        if (!isEmpty()) {
            int topEle = elements[--insertAtPos];
            elements[insertAtPos] = Integer.MIN_VALUE;
            size--;
            return topEle;
        }
        return Integer.MIN_VALUE;
    }

    @Override
    public boolean isEmpty() {
        return (size == 0);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int peek() {
        if(!isEmpty()) {
            return elements[insertAtPos-1];
        } else {
            return Integer.MIN_VALUE;
        }
    }

    @Override
    public void clear() {
        Arrays.fill(elements, Integer.MIN_VALUE);
        size = 0;
        insertAtPos = 0;
    }
}
