package stack;

public interface StackI {
    boolean push(int ele);
    int pop();
    boolean isEmpty();
    int size();
    int peek();
    void clear();
}
