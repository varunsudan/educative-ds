package queue;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CircularQueueTest {
    private CircularQueue cq;

    @Before
    public void setUp() {
        cq = new CircularQueue(5);
        //cq.bulkAdd(new int[]{1,2,3,4,5});
    }

    @After
    public void cleanUp() {
        cq = null;
    }

    @Test
    public void enqueue() {
        for (int i = 1; i <= 5; i++) {
            cq.enqueue(i);
            Assert.assertTrue(cq.size() == i);
            Assert.assertEquals(cq.front(), 1);
        }
    }

    @Test
    public void dequeue() {
        cq.bulkAdd(new int[]{1, 2, 3, 4, 5});
        int top = 0;
        for (int i = 1; i <= 5; i++) {
            top = cq.dequeue();
            Assert.assertEquals(cq.size(), (5 - i));
            Assert.assertEquals(top, i);
        }
    }

    @Test
    public void font() {
        Assert.assertEquals(cq.front(), Integer.MIN_VALUE);
        cq.bulkAdd(new int[]{1, 2, 3, 4});
        Assert.assertEquals(cq.front(), 1);
    }

    @Test
    public void size() {
        Assert.assertEquals(cq.size(), 0);
        cq.bulkAdd(new int[]{1, 2, 3, 4});
        Assert.assertEquals(cq.size(), 4);
    }

    @Test
    public void isEmpty() {
        Assert.assertTrue(cq.isEmpty());
        cq.bulkAdd(new int[]{1, 2, 3, 4});
        Assert.assertFalse(cq.isEmpty());
    }
}