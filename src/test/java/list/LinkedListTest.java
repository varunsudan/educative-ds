package list;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedListTest {

    LinkedList empty = new LinkedListImpl(null);
    LinkedList sizeEven = new LinkedListImpl().bulkAdd(new int[]{1, 2, 3, 4});
    LinkedList sizeOdd = new LinkedListImpl().bulkAdd(new int[]{1, 2, 3, 4, 5, 6, 7});
    LinkedList sizeOne = new LinkedListImpl().bulkAdd(new int[]{1});

    @Test
    public void getHeadWhenListIsEmpty() {
        Assert.assertNull(empty.getHead());
    }

    @Test
    public void getHeadWhenListIsNotEmpty() {
        Assert.assertTrue(sizeEven.getHead().getEle() == 1);
    }

    @Test
    public void getTaiWhenListIsEmpty() {
        Assert.assertNull(empty.getTail());
    }

    @Test
    public void getTaiWhenListIsNotEmpty() {
        Assert.assertTrue(sizeOdd.getTail().getEle() == 7);
    }

    @Test
    public void isEmpty() {
    }

    @Test
    public void print() {
    }

    @Test
    public void add() {
    }

    @Test
    public void delete() {
    }

    @Test
    public void size() {
    }

    @Test
    public void search() {
    }

    @Test
    public void bulkAdd() {
    }
}