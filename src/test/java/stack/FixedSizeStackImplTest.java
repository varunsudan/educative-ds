package stack;

import org.junit.Assert;
import org.junit.Test;

public class FixedSizeStackImplTest {

    private final int size = 5;

    @Test
    public void consolidatedTest() {

        StackI stack = new FixedSizeStackImpl(size);

        // Empty Stack test
        Assert.assertTrue(stack.isEmpty());
        // Pop & Top for empty stack
        Assert.assertEquals(stack.pop(), Integer.MIN_VALUE);
        Assert.assertEquals(stack.peek(), Integer.MIN_VALUE);

        // Push elements to an empty stack till its full
        for (int i = 1; i <= size; i++) {
            Assert.assertTrue(stack.push(i));
            Assert.assertFalse(stack.isEmpty());
            Assert.assertTrue(stack.size() == i);
        }

        // Push elements to full stack
        for (int i = 10; i <= size; i++) {
            Assert.assertFalse(stack.push(i));
            Assert.assertTrue(stack.size() == size);
            Assert.assertTrue(stack.peek() == 5);
        }

        Assert.assertTrue(stack.size() == size);
        Assert.assertFalse(stack.isEmpty());

        // Pop the stack till its empty
        for (int i = size; i > 0; i--) {
            Assert.assertFalse(stack.isEmpty());
            Assert.assertTrue(stack.size() == i);
            Assert.assertTrue(stack.pop() == i);
        }

        Assert.assertTrue(stack.size() == 0);
        Assert.assertTrue(stack.isEmpty());

        // Push few elements to stack
        for (int i = 1; i <= 10; i++) {
            stack.push(i);
            if (i >= 6) {
                Assert.assertTrue(stack.size() == 5);
            } else {
                Assert.assertTrue(stack.size() == i);
            }
        }
        stack.clear();
        Assert.assertTrue(stack.size() == 0);
        Assert.assertTrue(stack.pop() == Integer.MIN_VALUE);
        Assert.assertTrue(stack.peek() == Integer.MIN_VALUE);
    }
}