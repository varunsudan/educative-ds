package set;

import org.junit.Assert;
import org.junit.Test;

public class SetFixedSizeImplTest {

    SetFixedSizeImpl setImpl = new SetFixedSizeImpl(5);

    @Test
    public void add() {
        for(int i = 1; i <= 3; i++) {
            Assert.assertTrue(setImpl.add(i));
        }
        for(int i = 1; i <= 3; i++) {
            Assert.assertFalse(setImpl.add(i));
        }

        for(int i = 4; i <= 5 ; i++) {
            Assert.assertTrue(setImpl.add(i));
        }
        for(int i = 8; i <= 10; i++) {
            Assert.assertFalse(setImpl.add(i));
        }
    }

    @Test
    public void has() {
        for(int i = 1; i <= 5; i++) {
            Assert.assertTrue(setImpl.add(i));
            Assert.assertTrue(setImpl.has(i));
        }

        for(int i = 6; i <= 10; i++) {
            Assert.assertFalse(setImpl.has(i));
        }
    }

    @Test
    public void size() {
        Assert.assertEquals(setImpl.size(), 0);
        for(int i = 1; i <= 5; i++) {
            Assert.assertTrue(setImpl.add(i));
            Assert.assertEquals(setImpl.size(), i);
        }
    }

    @Test
    public void remove() {
        for(int i = 1; i <= 5; i++) {
            Assert.assertTrue(setImpl.add(i));
        }

        for(int i = 6; i <= 10; i++) {
            Assert.assertFalse(setImpl.remove(i));
            Assert.assertEquals(setImpl.size(),5);
        }
        for(int i = 1; i <= 5; i++) {
            Assert.assertTrue(setImpl.remove(i));
            Assert.assertEquals(setImpl.size(), (5-i));
        }
    }
}